<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------

namespace app\server\home;
use think\Db;
use GatewayWorker\Lib\Gateway;
/**
 * API控制器
 * @package app\cms\admin
 */
class Events extends Common
{
    /**
     */
    public function index()
    {
        return '123'; // 渲染模板
    }
    /**
     * 当客户端的连接上发生错误时触发
     * @param $connection
     * @param $code
     * @param $msg
     */
    public function onError($connection, $code, $msg)
    {
        echo "error $code $msg\n";
    }


    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     *
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {

//        file_put_contents('./test.log',
//            '来自：'.$_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT'].' 的客户：'.$client_id.' 在 '.date('Y-m-d H:i:s',
//                time()).' 与服务器连接成功!'.PHP_EOL,FILE_APPEND);

        // 向当前client_id发送数据
        toClient("init",$client_id,"连接成功");

//        // 向客服组发送
//        toGroup("group","kefu", '来自：'.$_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT'].' 的客户：'.$client_id.' 在 '.date('Y-m-d H:i:s', time()).' 与服务器连接成功!'.PHP_EOL,FILE_APPEND);
//        //Gateway::sendToAll("$client_id login\r\n");
    }

    /**
     * 当客户端发来消息时触发
     * @param int $client_id 连接id
     * @param mixed $message 具体消息
     */
    public static function onMessage($client_id, $message)
    {
        file_put_contents('./test.log','客户：'.$client_id.' 在 '.date('Y-m-d H:i:s',time()).'  发来消息：'.$message.PHP_EOL,FILE_APPEND);
        // 向所有人发送
        //Gateway::sendToAll("$client_id said $message\r\n");

        Gateway::sendToClient($client_id, "I've got your msg : $message --- $client_id\r\n");
    }

    /**
     * 当用户断开连接时触发
     * @param int $client_id 连接id
     */
    public static function onClose($client_id)
    {
//        file_put_contents('./test.log','客户：'.$client_id.' 断开连接'.PHP_EOL,FILE_APPEND);
        // 向所有人发送
//        GateWay::sendToAll(json_encode(['code'=>111,"msg"=>"对方离线","data"=>$client_id]));
    }


}